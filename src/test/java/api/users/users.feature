Feature: Users


Background:
* url 'https://demo.bvnk.co'

  @technical-debt
Scenario: signup user
    From: https://docs.bvnk.co/#user-signup
    Users act as account holders and need to be created as the first action.
    All accounts need to be linked to users, and Know Your Customer requirements
    centre around users. Country must be valid ISO2 code, a list of valid countries
    and their codes can be retrieved through GET /countries.
    The signup request should be done via a BVNK interface (web or mobile app)
    because it also uses HTTP authentication as a new customer would not have an account.

    A uuid will be returned which will be the identifier for the user in subsequent auth requests.
    Please see create auth request for more details.

    This request requires Basic Auth.

Given path 'account/signup'
    And header Authorization = call read('basic-auth.js') { username: 'bvnk', password: 'AFjDJurwxTNqqYfXsU6zXGvN' }
    And header Cache-Control = 'no-cache'
    #And header Postman-Token = 'a7d75f11-876a-9458-b195-43c967d62111'
    #TODO: a hack to ensure uniqueness quickly - pay of this technical debt
    And form field AccountHolderGivenName = call read('unique-hack-postfix.js')  {base:'Debug001'}
    #And form field AccountHolderGivenName = 'Debug001'
    And form field AccountHolderFamilyName = 'Account'
    And form field AccountHolderDateOfBirth = '1900-01-01'
    #TODO: a hack to ensure uniqueness quickly - pay of this technical debt
    And form field AccountHolderIdentificationNumber = call read('unique-hack-postfix.js')  {base:''}
    #And form field AccountHolderIdentificationNumber = '400000-0348-001'
    #TODO: a hack to ensure uniqueness quickly - pay of this technical debt
    And form field AccountHolderContactNumber1 = call read('unique-hack-phone.js')  {base:''}
    #And form field AccountHolderContactNumber1 = '+27324374192'
    #TODO: a hack to ensure uniqueness quickly - pay of this technical debt
    And form field AccountHolderContactNumber2 = call read('unique-hack-phone.js')  {base:''}
    #And form field AccountHolderContactNumber2 = '+27726374192'
    #TODO: a hack to ensure uniqueness quickly - pay of this technical debt
    And form field AccountHolderEmailAddress = call read('unique-hack-prefix.js')  {base:'@bankai.co'}
    #And form field AccountHolderEmailAddress = 'alice08@bankai.co'
    And form field AccountHolderAddressLine1 = 'Address 1'
    And form field AccountHolderAddressLine2 = 'Address 2'
    And form field AccountHolderAddressLine3 = 'Address 3'
    And form field AccountHolderPassword = 'password'
    And form field AccountHolderPostalCode = '1234'
    And form field AccountHolderCountry = 'GB'
    When method post
    Then status 200
    #And match response contains 'response'
    * match response == { response: '#uuid' }


#
