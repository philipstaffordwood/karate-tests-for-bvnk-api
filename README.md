# karate-tests-for-BvNK-api

Trying out Karate (https://github.com/intuit/karate) on the bank API

[Notes](./notes.md)

# Roadmap

*  [x] Get Karate Build/Test working
*  [x] Hello BVNK API
*  [ ] Cover a chunk of API
*  [ ] Use Jib (https://github.com/GoogleContainerTools/jib) to build to GitLab Repo as a containerised test runner